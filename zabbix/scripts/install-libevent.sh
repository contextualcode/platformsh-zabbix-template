#!/usr/bin/env bash

ZABBIX_DIR=/app/zabbix

cd ${ZABBIX_DIR}/src/libevent
./autogen.sh
./configure --prefix=${ZABBIX_DIR}/libevent
make install